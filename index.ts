import { output, all } from "@pulumi/pulumi";
import { ec2, iam, rds } from "@pulumi/aws";

// Create VPC and subnet.
const vpc = new ec2.Vpc("diamondhands-vpc", {
    cidrBlock: "10.0.0.0/16",
    enableDnsHostnames: true,
});

const subnet = new ec2.Subnet("diamondhands-web-subnet", {
    cidrBlock: "10.0.1.0/24",
    vpcId: vpc.id,
    availabilityZone: 'us-west-2a'
});

const subnet2 = new ec2.Subnet("diamondhands-db-subnet2", {
    cidrBlock: "10.0.2.0/24",
    vpcId: vpc.id,
    availabilityZone: 'us-west-2b'
});

// Create an an internet gateway.
const gateway = new ec2.InternetGateway("diamondhands-gateway", {
    vpcId: vpc.id,
});

// Create a route table.
const routes = new ec2.RouteTable("diamondhands-routes", {
    vpcId: vpc.id,
    routes: [
        {
            cidrBlock: "0.0.0.0/0",
            gatewayId: gateway.id,
        },
    ],
});

const routeTableAssociation = new ec2.RouteTableAssociation("diamondhands-route-table-association", {
    subnetId: subnet.id,
    routeTableId: routes.id,
});

const routeTableAssociation2 = new ec2.RouteTableAssociation("diamondhands-route-table-association2", {
    subnetId: subnet2.id,
    routeTableId: routes.id,
});

// Create a security group.
const ec2SG = new ec2.SecurityGroup("diamondhands-ec2-sg", {
    vpcId: vpc.id,
    ingress: [{
        fromPort: 80,
        toPort: 80,
        protocol: "tcp",
        cidrBlocks: ["0.0.0.0/0"],
    }, {
        fromPort: 22,
        toPort: 22,
        protocol: "tcp",
        cidrBlocks: ["0.0.0.0/0"],
    }],
    egress: [{
        fromPort: 80,
        toPort: 80,
        protocol: "tcp",
        cidrBlocks: ["0.0.0.0/0"],
    }, {
        fromPort: 443,
        toPort: 443,
        protocol: "tcp",
        cidrBlocks: ["0.0.0.0/0"],
    }],
});

const ec2RDSSG = new ec2.SecurityGroup("diamondhands-ec2-rds-sg", {
    vpcId: vpc.id
});

const rdsEC2SG = new ec2.SecurityGroup("diamondhands-rds-ec2-sg", {
    vpcId: vpc.id
});

const ec2RDSSGR = new ec2.SecurityGroupRule("diamondhands-to_RDS", {
    type: "egress",
    fromPort: 5432,
    toPort: 5432,
    protocol: "tcp",
    securityGroupId: ec2RDSSG.id,
    sourceSecurityGroupId: rdsEC2SG.id
})

const rdsEC2SGR = new ec2.SecurityGroupRule("diamondhands-from_RDS", {
    type: "ingress",
    fromPort: 5432,
    toPort: 5432,
    protocol: "tcp",
    securityGroupId: rdsEC2SG.id,
    sourceSecurityGroupId: ec2RDSSG.id
})

// Create IAM user and the SSH key.
const iamUser = new iam.User("diamondhands-iam-user", {
    path: "/system/",
});

const sshKey = new ec2.KeyPair("diamondhands-ssh-key", {
    keyName: "diamondhands-ssh-key",
    publicKey: "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICBH4iHPxQqTsR4MAiHfOuBJwxV+jtkHg0av0XnZ+1uy",
});

const subnetGroup = new rds.SubnetGroup("diamondhands-db-subnet-group", {
    subnetIds: [subnet.id, subnet2.id]
})

const dbname = "diamondhandsdb"
const dbuser = "diamondhandsapp"
const dbpass = "t0th3m00n"

const db = new rds.Instance("diamondhands-db", {
    allocatedStorage: 5,
    engine: "postgres",
    instanceClass: "db.t3.micro",
    storageType: "standard",
    dbName: dbname,
    username: dbuser,
    password: dbpass,
    vpcSecurityGroupIds: [rdsEC2SG.id],
    dbSubnetGroupName: subnetGroup.name,
    availabilityZone: "us-west-2a",
    skipFinalSnapshot: true,
    backupRetentionPeriod: 0,
});

export const dbUrl = all([db.address])
    .apply(([endpoint]) => `ecto://${dbuser}:${dbpass}@${endpoint}/${dbname}`);

const userdata = all([dbUrl])
    .apply(([url]) => `#!/bin/bash
    sudo amazon-linux-extras install -y docker
    sudo systemctl enable docker
    sudo systemctl start docker
    sudo docker pull sparkedev/diamondhands
    sudo docker run -d --name=diamondhands --restart=always -e DATABASE_URL=${url} -e SECRET_KEY_BASE=XEx2tegEZ/+c419u/P6925FuHnthKKV0pHM5XdovZulUM/B5S5o+J5HcsMUyYzfD -e PHX_HOST=localhost -p 80:4000 sparkedev/diamondhands
    `)

const amiId = output(ec2.getAmi({
    filters: [
        {
            name: "name",
            values: ["amzn2-ami-kernel-5.10-hvm-*-x86_64-gp2"],
        },
    ],
    mostRecent: true,
    owners: ["amazon"],
})).apply(result => result.id);

// Create an EC2 instance.
const server = new ec2.Instance("diamondhands-ec2", {
    instanceType: "t2.micro",
    ami: amiId,
    vpcSecurityGroupIds: [ec2SG.id, ec2RDSSG.id],
    subnetId: subnet.id,
    keyName: sshKey.keyName,
    associatePublicIpAddress: true,
    userData: userdata,
    availabilityZone: "us-west-2a",
});

export const publicIP = server.publicIp;